//
//  AppDelegate.h
//  TtestGitProject
//
//  Created by maowangxin on 13-12-11.
//  Copyright (c) 2013年 maowangxin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
